pipeline {
    agent any

    parameters{
        string(
            name: "REGISTRY_URL", 
            defaultValue: "registry-itau.mastertech.com.br",
            description: "Endereço do registry")
        string(
            name: "DOCKER_IMAGE", 
            defaultValue: "registry-itau.mastertech.com.br/imagem-doacao-app",
            description: "Imagem do Docker")
        string(
            name: "APPNAME", 
            defaultValue: "itaujuntos-dev-nsp")
    }

    post {
        always {
            cleanWs()
        }
        failure {
            updateGitlabCommitStatus name: "build", state: "failed"
        }
        success {
            updateGitlabCommitStatus name: "build", state: "success"
        }
    }
    options {
        gitLabConnection("gitlab")
    }

    stages{
        stage("Gerar Pacote") {
            steps {
                echo "<--- Gerando pacote --->"
                sh "./mvnw package -DskipTests"
                echo "<--- Pacote finalizado com sucesso! --->"
            }
        }

        stage("Publicar em desenvolvimento"){
			when{branch "develop"}
            stages{
                stage("Gerar Imagem Docker"){
                    steps{
                        script {
                            docker.withRegistry("https://${params.REGISTRY_URL}","registry_credential"){
                                def customImage=docker.build("${params.DOCKER_IMAGE}")
                                customImage.push("${env.BUILD_ID}")
                                customImage.push("latest")
                                echo "<--- Imagem dev ${params.DOCKER_IMAGE}:${env.BUILD_ID} gerada com sucesso!--->"
                            }
                        }
                    }
                }
                stage("Deploy"){
                    steps{
                        echo "<--- Iniciando Rollout Deployment --->"
                        sh "kubectl rollout restart -n ${params.APPNAME} deployment/itaujuntos-doacao-app-deployment"
                        echo "<--- Rollout dev finalizado com sucesso! --->"
                    }
                }
            }
        }
		stage("Publicar em prod"){
			when{branch "master"}
            stages{
                stage("Gerar Imagem Docker"){
                    steps{
                        script {
                            docker.withRegistry("https://${params.REGISTRY_URL}","registry_credential"){
                                def customImage=docker.build("${params.DOCKER_IMAGE}")
                                customImage.push("${env.BUILD_ID}")
                                customImage.push("latest")
                                echo "<--- Imagem prod ${params.DOCKER_IMAGE}:${env.BUILD_ID} gerada com sucesso!--->"
                            }
                        }
                    }
                }
                stage("Deploy"){
                    steps{
                        echo "<--- Iniciando Rollout Deployment --->"
                        sh "kubectl rollout restart -n ${params.APPNAME} deployment/itaujuntos-doacao-app-deployment"
                        echo "<--- Rollout prod finalizado com sucesso! --->"
                    }
                }
            }
        }
    }
}

