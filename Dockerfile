FROM openjdk:8-jre-alpine
COPY target/doacao-app-*.jar doacao-app.jar
COPY /appagent/ ./
CMD [ "java", "-javaagent:/javaagent.jar", "-jar", "doacao-app.jar"]
