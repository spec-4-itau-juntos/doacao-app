package br.com.itau.doacao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.itau.doacao.entity.Doacao;

public interface DoacaoRepository extends CrudRepository <Doacao, Long> {
	
	
	 String QUERY_BUSCA_DOACOES_CPF=" SELECT * from doacoes do where do.cpf_doador = :cpf";
	 
	 @Query(value=QUERY_BUSCA_DOACOES_CPF,nativeQuery = true)
	 List<Doacao> buscaDoacoesPorCPF(@Param("cpf") String cpf);
	 
	 String QUERY_BUSCA_DOACOES_CAMPANHA=" SELECT * from doacoes do where do.id_campanha = :idCampanha";
	 
	 @Query(value=QUERY_BUSCA_DOACOES_CAMPANHA, nativeQuery = true)
	 List<Doacao> buscaDoacoesPorCampanha(@Param("idCampanha") Long idCampanha);
	 

	 
}
