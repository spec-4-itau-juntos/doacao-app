package br.com.itau.doacao.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import br.com.itau.doacao.entity.dto.RequestDoacao;
import br.com.itau.doacao.entity.dto.ResponseDoacao;
import br.com.itau.doacao.entity.dto.ResponseDoacaoCampanha;
import br.com.itau.doacao.entity.dto.ResponseDoacaoResumo;
import br.com.itau.doacao.entity.mapper.DoacaoMapper;
import br.com.itau.doacao.service.DoacaoService;

@RestController
@RequestMapping ("/v1/doacao")
@CrossOrigin
public class DoacaoController {
	
	@Autowired
	DoacaoService DoacaoService;
	
	Logger logger = LoggerFactory.getLogger(DoacaoController.class);

	
    @GetMapping("/{cpf}")
    public ResponseEntity<List<ResponseDoacaoCampanha>> buscaDoacoesPorCPF(@Valid @PathVariable(value="cpf") String cpf) {
    	
    	logger.info("Busca campanhas pelo CPF " + cpf);
    	
    	
    	List<ResponseDoacaoCampanha> listaDoacoes= DoacaoMapper.toListResponseDoacaoCampanha(
    			DoacaoService.buscaDoacoesPorCPF(cpf));
    	
    	if(listaDoacoes!=null && !listaDoacoes.isEmpty()) {
        	logger.info("Doações encontradas para o CPF " + cpf);
    		return ResponseEntity.ok(listaDoacoes);
    	}
    	
    	logger.info("Doações não encontradas para o CPF " + cpf);
    	return ResponseEntity.noContent().build();
    	
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseDoacao insereDoacao(@RequestBody @Valid RequestDoacao requestDoacao){
    	
    	logger.info("Inserindo Doacao");
    	
    	ResponseDoacao responseDoacao= DoacaoMapper.toResponseDoacao(
                DoacaoService.criarDoacao(
                        DoacaoMapper.toDoacao(requestDoacao)));
    	
    	
    	logger.info("Doacao salva com sucesso com   : " + responseDoacao);
    	
    	return responseDoacao;
        
    }
    
    @GetMapping("/campanha/{id}")
    public ResponseEntity<List<ResponseDoacaoResumo>> buscaDoacoesPorCampanha(@Valid @PathVariable(value="id") Long idCampanha){
    	logger.debug("Buscando Doacao");
    	
    	List<ResponseDoacaoResumo> listaDoacoes = DoacaoMapper.toListResponseDoacao(
    			DoacaoService.buscaDoacoesPorCampanha(idCampanha));
    	
    	if(listaDoacoes!=null && !listaDoacoes.isEmpty()) {
    		logger.info("Doações encontradas para a campanha " + idCampanha);
    		return ResponseEntity.ok(listaDoacoes);
    	}
    	
    	logger.info("Doações não encontradas para a campanha " + idCampanha);
    	return ResponseEntity.noContent().build();
    }


}
