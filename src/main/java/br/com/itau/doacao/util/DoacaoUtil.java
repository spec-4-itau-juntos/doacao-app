package br.com.itau.doacao.util;

import java.util.List;

import br.com.itau.doacao.client.Campanha;
import br.com.itau.doacao.entity.Doacao;
import br.com.itau.doacao.entity.dto.DetalhesDoacao;

public class DoacaoUtil {
	

	public static void adicionaDetalheDoacao(List<DetalhesDoacao> listaDetalhe, Campanha campanha, Doacao doacao) {		
		
		DetalhesDoacao detalheDoacao;
		detalheDoacao = new DetalhesDoacao();
		detalheDoacao.setId(doacao.getId());
		detalheDoacao.setData(doacao.getData_doacao());
		detalheDoacao.setIdCampanha(doacao.getId_campanha());
		detalheDoacao.setValor(doacao.getValor());		
		detalheDoacao.setMeta(campanha.getMeta());
		detalheDoacao.setNome(campanha.getNome());
		
		
		listaDetalhe.add(detalheDoacao);
	}

}
