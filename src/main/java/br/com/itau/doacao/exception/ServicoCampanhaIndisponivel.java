package br.com.itau.doacao.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;


@ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE, reason = "Serviço de campanha indisponível.")
public class ServicoCampanhaIndisponivel extends RuntimeException{
	
	

}
