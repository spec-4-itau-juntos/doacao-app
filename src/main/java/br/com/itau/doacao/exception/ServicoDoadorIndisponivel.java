package br.com.itau.doacao.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;


@ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE, reason = "Serviço de doador indisponível.")
public class ServicoDoadorIndisponivel extends RuntimeException{
	
	

}
