package br.com.itau.doacao.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.doacao.client.Campanha;
import br.com.itau.doacao.client.CampanhaClient;
import br.com.itau.doacao.client.DoadorClient;
import br.com.itau.doacao.entity.Doacao;
import br.com.itau.doacao.entity.dto.DetalhesDoacao;
import br.com.itau.doacao.repositories.DoacaoRepository;
import br.com.itau.doacao.util.DoacaoUtil;

@Service
public class DoacaoService {
	
	@Autowired
	private DoacaoRepository doacaoRepository;
	
	@Autowired
	private CampanhaClient campanhaClient;
	
	@Autowired
	private DoadorClient doadorClient;
	
	@Value( "${campanha.valida.prazo}" )
	private Boolean flagValidaDataCampanha;
	
	@Value( "${doacao.agenda.pagamento}" )
	private Boolean flagPagamentoAgendado;
	
	@Value ("${doacao.pagamento.retroativo}")
	private Boolean flagPagamentoRetroativo;
	
	Logger logger = LoggerFactory.getLogger(DoacaoService.class);
	
	
	public List<DetalhesDoacao> buscaDoacoesPorCPF(String cpf){	
		
		doadorClient.recuperaDoador(cpf);
		
		logger.info("Doador validado com sucesso");
		
		List<Doacao> listDoacao = doacaoRepository.buscaDoacoesPorCPF(cpf);
		List<DetalhesDoacao> listaDetalhe = new ArrayList<DetalhesDoacao>();
		Map<Long,Campanha> mapCampanhas = new HashMap<Long,Campanha>();
		Campanha campanha;
		
		for (Doacao doacao : listDoacao) {
			
			if(doacao!=null && doacao.getId_campanha() !=null) {				
				campanha = recuperaDetalheCampanha(mapCampanhas, doacao.getId_campanha());				
				DoacaoUtil.adicionaDetalheDoacao(listaDetalhe, campanha, doacao);
			}
		}
		
		logger.info("Fim da busca por doacoes com sucesso");

		return listaDetalhe;
	}
	
	public List<Doacao> buscaDoacoesPorCampanha (Long idCampanha){
		logger.debug("Campanha buscada " + idCampanha);
		return doacaoRepository.buscaDoacoesPorCampanha(idCampanha);
	}
	

	public Doacao criarDoacao(Doacao doacao){		
		verificaDoador(doacao);
		verficaCampanhaValida(doacao);		
		verificaDataDoacao(doacao.getData_doacao());
		return doacaoRepository.save(doacao);
	}


	private Campanha recuperaDetalheCampanha(Map<Long, Campanha> mapCampanhas, Long idCampanha) {
		Campanha campanha;		
		logger.debug("Campanha buscada " + idCampanha);
		if(mapCampanhas.get(idCampanha)==null) {
			campanha = new Campanha();
			campanha =campanhaClient.buscaCampanha(idCampanha);
			logger.debug("Campanha encontrada " + campanha.getNome());
			mapCampanhas.put(campanha.getId(), campanha);
		}else {
			campanha = mapCampanhas.get(idCampanha);
			logger.debug("Campanha ja no cache  " + campanha.getNome());
		}	
		return campanha;
	}

	
	private void verificaDoador(Doacao doacao) {
		logger.info("Validando doador");
		doadorClient.recuperaDoador(doacao.getCpf_doador());
		logger.info("Doador validado com sucesso");
	}
	
	
	private void verficaCampanhaValida(Doacao doacao) {	
		Campanha campanha = campanhaClient.buscaCampanha(doacao.getId_campanha());	
		logger.debug ("Validando data fim da campanha " + campanha.getDataFim() + "para doação " + doacao.getData_doacao());
		
		if(campanha.getDataFim().isBefore(doacao.getData_doacao())) {
			logger.warn("Campanha fora do prazo para doação");			
			throw new ResponseStatusException(
			          HttpStatus.UNPROCESSABLE_ENTITY, "Campanha fora do prazo para doação: Data fim: " + campanha.getDataFim());
		}
		
		logger.info("Campanha validada com sucesso nome=" + campanha.getNome() + " id=" + campanha.getId());

	}
	
	private void verificaDataDoacao(LocalDate dataDoacao) {		
		
		if(!flagPagamentoRetroativo && dataDoacao.isBefore(LocalDate.now())) {
			logger.warn ("Data da doação menor que hoje");
			throw new ResponseStatusException(
			          HttpStatus.UNPROCESSABLE_ENTITY, "Data da doacao menor que hoje");
		}
				
		if(!flagPagamentoAgendado && dataDoacao.isAfter(LocalDate.now())) {
			logger.warn ("Pagamento agendado não permitido");
			throw new ResponseStatusException(
			          HttpStatus.UNPROCESSABLE_ENTITY, "funcao de agendamento de doação desabilitada. A data deve ser hoje");	
		}
		
	}

}
