package br.com.itau.doacao.entity.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonFormat;

public class RequestDoacao {

	@NotNull(message = "O cpf do doador não pode ser nulo ou vazio.")
	@NotBlank (message = "O cpf do doador não pode ser nulo ou vazio.")
	@CPF(message = "numero de cpf invalido")
    private String cpfDoador;

    @NotNull(message = "O valor da doação não pode ser nulo ou vazio.")
    private Double valor;

    @NotNull(message = "O id da campanha do investimento não pode ser nulo ou vazio.")
    private Long idCampanha;

    @NotNull(message = "A data precisa ser preenchida. dd/mm/yyyy")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", locale = "pt-BR", timezone = "Brazil/East")
    private LocalDate dataDoacao;

	public String getCpfDoador() {
		return cpfDoador;
	}

	public void setCpfDoador(String cpfDoador) {
		this.cpfDoador = cpfDoador;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Long getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(Long idCampanha) {
		this.idCampanha = idCampanha;
	}

	public LocalDate getDataDoacao() {
		return dataDoacao;
	}

	public void setDataDoacao(LocalDate dataDoacao) {
		this.dataDoacao = dataDoacao;
	}
    
    
    
	
}
