package br.com.itau.doacao.entity.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.itau.doacao.entity.Doacao;
import br.com.itau.doacao.entity.dto.DetalhesDoacao;
import br.com.itau.doacao.entity.dto.RequestDoacao;
import br.com.itau.doacao.entity.dto.ResponseDoacao;
import br.com.itau.doacao.entity.dto.ResponseDoacaoCampanha;
import br.com.itau.doacao.entity.dto.ResponseDoacaoResumo;

public class DoacaoMapper {
	
	
	public static ResponseDoacao toResponseDoacao(Doacao doacao) {
		ResponseDoacao responseDoacao = new ResponseDoacao();
		responseDoacao.setCpfDoador(doacao.getCpf_doador());
		responseDoacao.setDataDoacao(doacao.getData_doacao());
		responseDoacao.setId(doacao.getId());
		responseDoacao.setIdCampanha(doacao.getId_campanha());
		responseDoacao.setValor(doacao.getValor());
		return responseDoacao;
	}
	
	
	public static Doacao toDoacao(RequestDoacao requestDoacao) {
		Doacao doacao = new Doacao();
		doacao.setCpf_doador(requestDoacao.getCpfDoador());
		doacao.setData_doacao(requestDoacao.getDataDoacao());
		doacao.setId_campanha(requestDoacao.getIdCampanha());
		doacao.setValor(requestDoacao.getValor());
		return doacao;
	}
	
	
	public static List<ResponseDoacaoCampanha> toListResponseDoacaoCampanha(List<DetalhesDoacao> listDoacao){
		List<ResponseDoacaoCampanha>   listResponseDoacao = new ArrayList<ResponseDoacaoCampanha>();
		if(listDoacao!=null && !listDoacao.isEmpty()) {
			listDoacao.forEach(
					n-> listResponseDoacao.add(
							new ResponseDoacaoCampanha(
									n.getId(),n.getNome(),n.getMeta(),n.getIdCampanha(),n.getValor(),n.getData())));
					
		}
		return listResponseDoacao;
	}
	
	
	public static List<ResponseDoacaoResumo> toListResponseDoacao(List<Doacao> listDoacao){
		List<ResponseDoacaoResumo>   listResponseDoacao = new ArrayList<ResponseDoacaoResumo>();
		if(listDoacao!=null && !listDoacao.isEmpty()) {
			listDoacao.forEach(
					n-> listResponseDoacao.add(
							new ResponseDoacaoResumo(n.getId(),n.getCpf_doador(),n.getValor(),n.getData_doacao()
									)));									
		}
		return listResponseDoacao;
	}
	
	 

}
