package br.com.itau.doacao.entity.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ResponseDoacaoCampanha {
	
	private Long id;
	private String nome;
	private Double meta;
	private Long idCampanha;
	private Double valor;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", locale = "pt-BR", timezone = "Brazil/East" )
	private LocalDate dataDoacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getMeta() {
		return meta;
	}

	public void setMeta(Double meta) {
		this.meta = meta;
	}

	public Long getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(Long idCampanha) {
		this.idCampanha = idCampanha;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public LocalDate getDataDoacao() {
		return dataDoacao;
	}

	public void setDataDoacao(LocalDate dataDoacao) {
		this.dataDoacao = dataDoacao;
	}

	public ResponseDoacaoCampanha() {
		super();		 
	}

	public ResponseDoacaoCampanha(Long id, String nome, Double meta, Long idCampanha, Double valor,
			LocalDate dataDoacao) {
		super();
		this.id = id;
		this.nome = nome;
		this.meta = meta;
		this.idCampanha = idCampanha;
		this.valor = valor;
		this.dataDoacao = dataDoacao;
	}

    
    
}
