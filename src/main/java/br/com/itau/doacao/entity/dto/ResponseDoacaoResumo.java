package br.com.itau.doacao.entity.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ResponseDoacaoResumo {

private Long id;
	
    private String cpfDoador;

    private Double valor;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", locale = "pt-BR", timezone = "Brazil/East" )
    private LocalDate dataDoacao;

    
	public String getCpfDoador() {
		return cpfDoador;
	}

	public void setCpfDoador(String cpfDoador) {
		this.cpfDoador = cpfDoador;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public LocalDate getDataDoacao() {
		return dataDoacao;
	}

	public void setDataDoacao(LocalDate dataDoacao) {
		this.dataDoacao = dataDoacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ResponseDoacaoResumo(Long id, String cpfDoador, Double valor, LocalDate dataDoacao) {
		super();
		this.id = id;
		this.cpfDoador = cpfDoador;
		this.valor = valor;
		this.dataDoacao = dataDoacao;
	}

	public ResponseDoacaoResumo() {
		super();
	}
	
	
}
