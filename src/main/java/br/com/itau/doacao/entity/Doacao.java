package br.com.itau.doacao.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table (name = "doacoes")
public class Doacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "O cpf do doador não pode ser nulo ou vazio.")
    private String cpf_doador;

    @NotNull(message = "O valor da doação não pode ser nulo ou vazio.")
    private Double valor;

    @NotNull(message = "O id da campanha do investimento não pode ser nulo ou vazio.")
    private Long id_campanha;

    @NotNull(message = "A data precisa ser preenchida.")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "pt-BR", timezone = "Brazil/East" )
    private LocalDate data_doacao;


    public Doacao() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCpf_doador() {
        return cpf_doador;
    }

    public void setCpf_doador(String cpf_doador) {
        this.cpf_doador = cpf_doador;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Long getId_campanha() {
        return id_campanha;
    }

    public void setId_campanha(Long id_campanha) {
        this.id_campanha = id_campanha;
    }

    public LocalDate getData_doacao() {
        return data_doacao;
    }

    public void setData_doacao(LocalDate data_doacao) {
        this.data_doacao = data_doacao;
    }
}

