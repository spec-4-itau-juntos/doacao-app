package br.com.itau.doacao.entity.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;


public class ResponseDoacao {

	private Long id;
	
    private String cpfDoador;

    private Double valor;

    private Long idCampanha;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", locale = "pt-BR", timezone = "Brazil/East" )
    private LocalDate dataDoacao;

    
	public String getCpfDoador() {
		return cpfDoador;
	}

	public void setCpfDoador(String cpfDoador) {
		this.cpfDoador = cpfDoador;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Long getIdCampanha() {
		return idCampanha;
	}

	public void setIdCampanha(Long idCampanha) {
		this.idCampanha = idCampanha;
	}

	public LocalDate getDataDoacao() {
		return dataDoacao;
	}

	public void setDataDoacao(LocalDate dataDoacao) {
		this.dataDoacao = dataDoacao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ResponseDoacao(Long id, String cpfDoador,Double valor, Long idCampanha, LocalDate dataDoacao) {
		super();
		this.id = id;
		this.cpfDoador = cpfDoador;
		this.valor = valor;
		this.idCampanha = idCampanha;
		this.dataDoacao = dataDoacao;
	}

	public ResponseDoacao() {
		super();
	}

	@Override
	public String toString() {
		return "ResponseDoacao [id=" + id + ", cpf_doador=" + cpfDoador + ", valor=" + valor + ", id_campanha="
				+ idCampanha + ", data_doacao=" + dataDoacao + "]";
	}
    
	
	
    
    
	
}
