package br.com.itau.doacao.client;

import br.com.itau.doacao.exception.ServicoCampanhaIndisponivel;

public class CampanhaClientFallback implements CampanhaClient{

	@Override
	public Campanha buscaCampanha(Long id) {
		throw new ServicoCampanhaIndisponivel();
	}

}
