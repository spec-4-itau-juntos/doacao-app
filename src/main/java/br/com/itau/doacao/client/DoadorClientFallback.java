package br.com.itau.doacao.client;

import br.com.itau.doacao.exception.ServicoDoadorIndisponivel;

public class DoadorClientFallback implements DoadorClient{

	@Override
	public Doador recuperaDoador(String id) {
		throw new ServicoDoadorIndisponivel();
	}

}
