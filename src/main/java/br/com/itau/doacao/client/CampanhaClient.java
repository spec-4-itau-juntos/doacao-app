package br.com.itau.doacao.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="itaujuntos-campanha-service")
public interface CampanhaClient {

	@GetMapping("/v1/campanha/{id}")
	Campanha buscaCampanha(@PathVariable(value="id") Long id); 
}
