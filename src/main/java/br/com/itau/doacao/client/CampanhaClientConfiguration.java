package br.com.itau.doacao.client;

import org.springframework.context.annotation.Bean;

import feign.Feign;
import feign.RetryableException;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;

public class CampanhaClientConfiguration {

	
	@Bean
	public Feign.Builder builder(){
		FeignDecorators decorator = FeignDecorators.builder()
				.withFallback(new CampanhaClientFallback(),RetryableException.class)
				.build();
	
		return Resilience4jFeign.builder(decorator);
	}

}
