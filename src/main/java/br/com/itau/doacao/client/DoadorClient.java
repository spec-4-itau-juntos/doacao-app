package br.com.itau.doacao.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name="itaujuntos-doador")
public interface DoadorClient {
	
	@GetMapping("/v1/cliente/{id}")
	Doador recuperaDoador(@PathVariable(value="id") String id);

}
