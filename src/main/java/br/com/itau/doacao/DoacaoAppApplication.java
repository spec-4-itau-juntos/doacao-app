package br.com.itau.doacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

import br.com.itau.util.handler.GlobalExceptionHandler;

@SpringBootApplication
@Import(GlobalExceptionHandler.class)
@EnableFeignClients
public class DoacaoAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(DoacaoAppApplication.class, args);
    }

}
