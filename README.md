# Itaú Juntos

## Descrição

Integração com o aplicativo do Itaú que permite correntistas fazerem doações para campanhas de projetos sociais liderados pelo banco.

## Doações

Serviço para efetivação das doações para uma campanha.


### Contratos Doações

## POST /v1/doacao - Adiciona uma doação à campanha

**Status Codes**:
  - 201: Doação criada
  - 400: Bad Request
  - 400: CPF inválido
  - 404: Usuário não encontrado
  - 404: Campanha não encontrada
  - 422: Campanha fora do prazo para receber doação (configurável)
  - 422: Data de doção menor que hoje (configurável)
  - 422: Não é permitido pagamento agendado (configurável)



    **Request**:

    ```Json
    {
        "cpf_doador": "52325362060",
        "valor": 1000.00,
        "id_campanha": 1,
        "data_doacao": "2021-12-25"
    }
    ```

    **Response**:

    ```Json
    {
        "id": 1,
        "cpf_doador": "52325362060",
        "valor": 1000.00,
        "id_campanha": 1,
        "data_doacao": "2021-12-25"
    }
    ```

## GET /v1/doacao/{cpf} - Retorna a lista de campanhas que o doador contribuiu

**Status Codes**
  - 200: OK
  - 404: Doador não encontrado
  - 204: Doações não encontradas
  - 404: Campanha não encontrada

  
    **Response**:

    ```Json
    [
        {  
            "id": 1,
            "nome": "Vacinação Malária",
            "meta": 15000.00,
            "id_campanha": 1,
            "valor": 100.00,
            "data_doacao": "2021-12-25"
        },
        {  
            "id": 2,
            "nome": "Vacinação Malária",
            "meta": 15000.00,
            "id_campanha": 1,
            "valor": 150.00,
            "data_doacao": "2021-11-25"
        }
    ]
    ```
## GET /v1/doacao/campanha/{id} - Retorna a lista de doação por Campanha

**Status Codes**
  - 200: OK
  - 400: Bad Request
  - 204: Campanha não encontrada

  
    **Response**:

    ```Json
    [
        {
            "id": 1,
            "cpf_doador": "52325362060",
            "valor": 70.54,
            "data_doacao": "2021-08-06"
        },
        {
            "id": 2,
            "cpf_doador": "52325362060",
            "valor": 70.54,
            "data_doacao": "2021-08-06"
        }    
    ]
    ```

